var util = require('../util');

var getMarkov = util.getMarkov;
var segmenter = util.segmenter;
var log = util.log;

var model = require('../model');
var SegmentModel = model.SegmentModel;
var RandomMessageModel = model.RandomMessageModel;

var RandomMessagePlugin = function() {
  RandomMessagePlugin.prototype.initialize.apply(this, arguments);
};

RandomMessagePlugin.prototype = {
  initialize: function(arg) {
    this.randomMessageModel = new RandomMessageModel();
  },

  onPrivMessage: function(bot, from, channel, message) {
    this.sayRandomMessage(bot, channel);
  },

  onPrivMessageAlways: function(bot, from, channel, message) {
    if (!/きりつぐ/.test(message)) {
      this.writeMarkov(from, channel, message);
      this.createRandomMessage();
    }
  },

  createRandomMessage: function() {
    var segmentModel = new SegmentModel;

    //始まりのセグメントを探す
    var selectRandomStartSegment = function() {
      var fiber = Fiber.current;
      var result = {
        err: null,
        docs: null
      };

      var callback = function(err, docs) {
        result.err = err;
        result.docs = docs;
        fiber.run();
      };

      segmentModel.selectRandomStartSegment(callback);
      Fiber.yield();

      return result;
    }

    //次のセグメントを探す
    var selectRandomNextSegment = function(segment) {
      var fiber = Fiber.current;
      var result = {
        err: null,
        docs: null
      };

      var callback = function(err, docs) {
        result.err = err;
        result.docs = docs;
        fiber.run();
      };

      segmentModel.selectRandomNextSegment(segment, callback);

      Fiber.yield();

      return result;
    }

    //ランダムメッセージを作成する
    Fiber(function() {
      var docs = [];
      var doc = null;

      var message = null;
      do {
        var result = doc === null ? selectRandomStartSegment() : selectRandomNextSegment(doc.next_segment);

        if (result.err) {
          return;
        }

        if (result.docs.length == 0) {
          return;
        }

        if (doc === null) {
          message = result.docs[0].segment;
        }

        doc = result.docs[0];
        docs.push(doc);
        message = message + doc.next_segment;

        if (message.length > segmentModel.maxRandomMessage) {
          log.info('RandomMessagePlugin : too long random message.');
          return;
        }
      } while (doc.end_count === 0);


      var randomMessageModel = new RandomMessageModel();
      randomMessageModel.write(message);

      return;
    }).run();
  },

  writeMarkov: function(from, channel, message) {
    var segments = (typeof message === 'string') ? segmenter.segment(message) : message;
    var markov = getMarkov(segments);

    var segmentModel = new SegmentModel();
    segmentModel.writeMarkov(markov, {
      channel: channel,
      from: from,
      callback: function(err) {
        if (err) {
          log.error(err.toString());
        }
      }
    });
  },

  //適当なランダムメッセージを作成して返す
  sayRandomMessage: function(bot, channel) {
    this.randomMessageModel.selectRandomMessage(function(err, docs) {
      if (err) {
        log.error(err.toString());
        return;
      }

      if (docs.length === 0) {
        log.warning("Couldn't find random message.");
        return;
      }

      bot.notice(channel, docs[0].message);
    });
  }
};

module.exports = {
  'RandomMessagePlugin' : RandomMessagePlugin
};

