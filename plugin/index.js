var MessageLogPlugin = require('./message_log').MessageLogPlugin;
var RandomMessagePlugin = require('./random_message').RandomMessagePlugin;

module.exports = {
  MessageLogPlugin: MessageLogPlugin,
  RandomMessagePlugin: RandomMessagePlugin
};

