var util = require('../util');

var log = util.log;
var segmenter = util.segmenter;

var model = require('../model');
var MessageModel = model.MessageModel;

var MessageLogPlugin = function() {
  MessageLogPlugin.prototype.initialize.apply(this, arguments);
};

MessageLogPlugin.prototype = {
  initialize: function(arg) {
    // message model for log
    this.messageModel = new MessageModel();
  },

  onPrivMessage: function(bot, from, channel, message) {
  },

  onPrivMessageAlways: function(bot, from, channel, message) {
    this.writeMessage(from, channel, message);
  },

  writeMessage: function(from, channel, message) {
    var segments = (typeof message === 'string') ? segmenter.segment(message) : message;

    //メッセージログを書き込む
    this.messageModel.write({
      from: from,
      channel: channel,
      priv_msg: segments
    },{
      callback: function(err) {
        if (err) {
          log.error(err.toString());
        }
      }
    });
  }
};

module.exports = {
  'MessageLogPlugin' : MessageLogPlugin
};

