var EventListener = function() {
  EventListener.prototype.initialize.apply(this, arguments);
};

EventListener.prototype = {
  initialize: function(arg) {
    if (!arg) { arg = {}; }
    this.eventListeners = {};
  },

  addEventListener: function(type, listener) {
    if (!this.eventListeners.hasOwnProperty(type)) {
      this.eventListeners[type] = [];
    }
    this.eventListeners[type].push(listener);
  },

  removeEventListener: function(type, listener) {
    if (!this.eventListeners.hasOwnProperty(type)) {
      return;
    }

    var index = this.eventListeners[type].indexOf(listener);
    if (index < 0) {
      return;
    }

    this.eventListeners[type].splice(index);
  },

  emitEvents: function(type, argv) {
    if (!this.eventListeners.hasOwnProperty(type)) {
      return;
    }

    var i = 0;
    for (i = 0; i < this.eventListeners[type].length; ++i) {
      this.eventListeners[type][i].apply(this, argv);
    }
  }
};

module.exports = {
  'EventListener' : EventListener
};

