var Log = require('log');
var TinySegmenter = require('../lib/tinysegmenter').TinySegmenter;
var segmenter = new TinySegmenter();

var inherit = function(o, base)
{
  for (var key in base.prototype) {
    if (!o.prototype[key]) {
      o.prototype[key] = base.prototype[key];
    }
  }
};

var getMarkov = function(data) {
  if (typeof data === 'string') {
    data = segmenter.segment(data);
  }

  var markov = {};

  for (var i = 0; i < data.length; ++i) {
    var seg = data[i];

    // 辞書をセグメント情報で初期化する
    if (!markov.hasOwnProperty(seg)) {
      markov[seg] = {
        next: {},
        start: false,
        end: false
      };
    }

    // 初期センテンスであれば重みを増やす
    if (i == 0) {
      markov[seg].start = true;
    }

    // 次のセグメントが存在すれば遷移先を追加
    if (i + 1 < data.length) {
      var next = markov[seg].next;
      var nextSeg = data[i + 1];

      // 遷移先が登録されていなければ重みを１で初期化
      if (!next.hasOwnProperty(nextSeg)) {
        next[nextSeg] = {
          start: false,
          end: false
        };
      }

      if (i == 0) {
        next[nextSeg].start = true;
      }

      // 最後であれば
      if (i + 2 === data.length) {
        next[nextSeg].end = true;
      }
    } else {
        markov[seg].end = true;
    }
  }

  return markov;
};

module.exports = {
  log: new Log(),
  getMarkov: getMarkov,
  segmenter: segmenter,
  inherit: inherit
};

