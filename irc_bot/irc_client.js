var Iconv = require('iconv').Iconv;
var Client = require('irc').Client;
var Fiber = require('fibers');

var EventListener = require('../util/event_listener').EventListener;

var util = require('../util');
var log = util.log;

var IrcClient = function() {
  IrcClient.prototype.initialize.apply(this, arguments);
};

IrcClient.prototype = {
  initialize: function(arg) {
    EventListener.prototype.initialize.apply(this);
    if (!arg) { arg = {}; }
    this.host = arg.host || '127.0.0.1';
    this.userName = arg.userName || 'kiritsugu';
    this.nickname = arg.nickname || 'kiritsugu';
    this.realName = arg.realName || 'Kiritsugu EMIYA';
    this.channels = arg.channels || ['#kiritsugu'];
    this.charset = arg.charset || 'ISO-2022-jp';

    this.ircEventPrefix = 'irc:';

    this.decodeString = (function(decoder) {
      return function(message) {
       return decoder.convert(message).toString();
      }
    })(new Iconv(this.charset, 'UTF-8'));

    this.encodeString = (function(encoder) {
      return function(message) {
       return encoder.convert(message);
      }
    })(new Iconv('UTF-8', this.charset));

    var option = { channels: this.channels, userName: this.userName, realName: this.realName, autoConnect: false };

    this.client = new Client(this.host, this.nickname, option);

    // bind irc messages
    this.bindIrcEventListener('registered');
    this.bindIrcEventListener('error');
    this.bindIrcEventListener('message');

    // add listeners
    this.addIrcEventListener('registered', this.onIrcRegisterd);
    this.addIrcEventListener('error', this.onIrcError);
    this.addIrcEventListener('message', this.onIrcMessage);

    // add connection end event
    this.addEventListener('end', this.onEnd);
  },

  isConnected: function() {
    return this.client.conn.destroyed === false;
  },

  notice: function(channel, message) {
    this.client.notice(channel, this.encodeString(message));
  },

  encodeString: function(message) {
    // ダミー実装
    return (new Iconv('UTF-8', this.charset)).convert(message);
  },

  decodeString: function(message) {
    // ダミー実装
    return (new Iconv(this.charset, 'UTF-8')).convert(message).toString();
  },

  start: function() {
    log.info('IRC : connecting.');
    this.client.connect();
  },

  bindIrcEventListener: function(type) {
    var that = this;
    this.client.addListener(type, function() {
      that.emitEvents(that.ircEventPrefix + type, arguments);
    });
  },

  addIrcEventListener: function(type, listener) {
    this.addEventListener(this.ircEventPrefix + type, listener);
  },

  removeIrcEventListener: function(type, listener) {
    this.removeEventListener(this.ircEventPrefix + type, listener);
  },

  onIrcError: function(message) {
    log.error(message.toString());
  },

  onIrcRegisterd: function(message) {
    var that = this;
    this.client.conn.addListener('end', function() {
      that.emitEvents('end');
    });

    log.notice('IRC : registered.');
  },

  onIrcMessage: function(from, channel, message) {
  },

  onEnd: function() {
    log.notice('IRC : disconnected');
  }
};
util.inherit(IrcClient, EventListener);

module.exports = {
  'IrcClient' : IrcClient
};

