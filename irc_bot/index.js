var IrcBot = require('./irc_bot').IrcBot;

module.exports = {
  'IrcClient' : require('./irc_client').IrcClient,
  'IrcBot' : require('./irc_bot').IrcBot
};

