var Iconv = require('iconv').Iconv;
var Client = require('irc').Client;
var Fiber = require('fibers');

var IrcClient = require('./irc_client').IrcClient;

var plugins = require('../plugin');

var util = require('../util');
var log = util.log;

var IrcBot = function() {
  IrcBot.prototype.initialize.apply(this, arguments);
};

IrcBot.prototype = {
  initialize: function(arg) {
    IrcClient.prototype.initialize.apply(this, [arg]);

    this.plugins = {};
    for (var name in plugins) {
      if (!plugins.hasOwnProperty(name)) {
        break;
      }

      log.info('IRCBOT : load (' + name + ')');
      this.plugins[name] = new plugins[name];
    }
  },

  onIrcMessage: function(from, channel, message) {
    IrcClient.prototype.onIrcMessage.apply(this, arguments);
    message = this.decodeString(message);

    log.info('IRCBOT : privmsg(' + from + '@' + channel + ')' + message);

    for (var name in this.plugins) {
      if (!this.plugins.hasOwnProperty(name)) {
        continue;
      }

      var plugin = this.plugins[name];

      plugin.onPrivMessageAlways(this, from, channel, message);
    }

    if (/きりつぐ/.test(message) && this.plugins.RandomMessagePlugin) {
      this.plugins.RandomMessagePlugin.onPrivMessage(this, from, channel, message);
    }
  }
};

util.inherit(IrcBot, IrcClient);

module.exports = {
  'IrcBot' : IrcBot
};

