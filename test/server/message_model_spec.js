var mongoose = require('mongoose')

var model = require('../../model');
var MessageModel = model.MessageModel;

var util = require('../../util');

var host = '127.0.0.1';
var database = 'test';
var port = 27017;

jasmine.DEFAULT_TIMEOUT_INTERVAL = 500;

describe('メッセージモデル', function(done) {
  beforeEach(function(done) {
    mongoose.connect( host, database, port, function(err) {
      (new MessageModel()).DbModel.remove(function(err) {
        var value = {
          from : 'test user1',
          channel : '#tech_ch1',
          priv_msg : ['This', 'is', 'a', 'pen']
        };

        var opt = {
          callback : function(err) {
            done();
          }
        };

        (new MessageModel).write(value, opt);
      });
    });
  });

  afterEach(function(done) {
    mongoose.disconnect(function(err) {
      done();
    });
  });

  it('書き込み成功', function(done) {
    var value = {
      from : 'test user2',
      channel : '#tech_ch2',
      priv_msg : ['This', 'is', 'a', 'pen']
    };

    var opt = {
      callback : function(err) {
        expect(err).toBeFalsy();
        done();
      }
    };

    (new MessageModel).write(value, opt);
  });

  it('データ検索', function(done) {
    var messageModel = new MessageModel();
    messageModel.find(function(err, docs){
      expect(err).toBeFalsy();
      expect(docs.length).toEqual(1);
      expect(typeof docs[0].from).toEqual('string');
      expect(typeof docs[0].channel).toEqual('string');
      expect(typeof docs[0].priv_msg.length).toBeDefined();
      done();
    });
  });
});

