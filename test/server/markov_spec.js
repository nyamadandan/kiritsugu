var getMarkov = require('../../util').getMarkov;

describe('マルコフ連鎖', function() {
  var message = '隣の客はよく書き食う客だ';
  it('始まりと終わりのフラグを持つ', function() {
    var markov = getMarkov(message);

    var startCount = 0;
    var endCount = 0;
    for( var segment in markov ) {
      if(!markov.hasOwnProperty(segment)) {
        continue;
      }

      for( var nextSegment in markov[segment].next ) {
        if(!markov[segment].next.hasOwnProperty(nextSegment)) {
          continue;
        }

        if(markov[segment].next[nextSegment].start) {
          startCount = startCount + 1;
        }

        if(markov[segment].next[nextSegment].end) {
          endCount = endCount + 1;
        }
      }
    }

    expect(startCount).toEqual(1);
    expect(endCount).toEqual(1);
  });
});

