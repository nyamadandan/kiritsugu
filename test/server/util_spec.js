var util = require('../../util');

jasmine.DEFAULT_TIMEOUT_INTERVAL = 100;

describe('ユーティリティ', function() {
  var BaseClass = function() {
    this.prototype.initialize.apply(this, arguments);
  };

  BaseClass.prototype = {
    initialize : function(x) {
      this.value = x;
    },

    method1 : function() {
      return 1;
    },

    method2 : function() {
      return 2;
    },

    method3 : function() {
      return this.value;
    }
  };

  var SubClass = function() {
    BaseClass.prototype.initialize.apply(this, arguments);
    SubClass.prototype.initialize.apply(this, arguments);
  };

  SubClass.prototype = {
    initialize : function(x) {
      this.value = x * 2;
    },

    method2 : function() {
      return 1;
    },

    method4 : function() {
      return 4;
    }
  };
  util.inherit(SubClass, BaseClass);

  var obj = null;
  beforeEach(function() {
    obj = new SubClass(1);
  });

  afterEach(function() {
    obj = null;
  });

  it('スーパークラスのメソッドを呼ぶ', function() {
    expect(obj.method1()).toEqual(1);
  });

  it('メソッドのオーバーライドを行う', function() {
    expect(obj.method2()).toEqual(1);
  });

  it('コンストラクタのオーバーライドを行う', function() {
    expect(obj.method3()).toEqual(2);
  });

  it('メソッドの新規追加を行う', function() {
    expect(obj.method4()).toEqual(4);
  });
});

