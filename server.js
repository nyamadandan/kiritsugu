/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , http = require('http')
  , mongoose = require('mongoose')
  , path = require('path')
  , config = require('config');

var IrcBot = require('./irc_bot').IrcBot;

var util = require('./util');
var log = util.log;

var app = express();

app.configure(function() {
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));

  routes = routes(config);
});

app.configure('development', function() {
  app.use(express.errorHandler());
});

app.get('/', routes.index);

app.get('/irc_bots', routes.irc_bots.show);
app.get('/irc_bots/:action', routes.irc_bots.get);
app.post('/irc_bots/:action', routes.irc_bots.post);

app.get('/messages', routes.messages.show);
app.get('/random_messages', routes.random_messages.show);
app.get('/segments', routes.segments.show);

var ircBot = new IrcBot();
var startBotClient = function() {
  var onRegistered = function() {
    ircBot.removeIrcEventListener('registered', onRegistered);
  }
  ircBot.addIrcEventListener('registered', onRegistered);
  ircBot.start();
};

var startHttpServer = function() {
  http.createServer(app).listen(app.get('port'), function() {
    log.notice('Express server listening on port ' + app.get('port'));
  });
};

var startMongooseConnection = function() {
  var mongooseConnection = mongoose.connect(
    config.mongoose.host,
    config.mongoose.database,
    config.mongoose.port,
    function(err) {
      if (err) {
        log.error(err.toString());
      } else {
        log.notice('MongoDB : connected.');
      }
    }
  );
};

Fiber(function() {
  startMongooseConnection();
  startHttpServer();
  startBotClient();
}).run();

