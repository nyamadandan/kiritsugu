var mongoose = require('mongoose');

var Fiber = require('fibers');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var util = require('../util');
var BaseModel = require('./base_model').BaseModel;

var MessageModel = function() {
  MessageModel.prototype.initialize.apply(this, arguments);
};

MessageModel.prototype = {
  initialize : function(connection) {
    var schema = new Schema({
      from: {
        type: String,
        required : true
      },
      channel: {
        type: String,
        required : true
      },
      priv_msg: [{
        type : String,
      }],
      insert_date: {
        type: Date,
        default : BaseModel.prototype.now,
        required : true
      }
    });

    BaseModel.prototype.initialize.apply(this, [connection, 'message', schema]);
  }
};

util.inherit(MessageModel, BaseModel);

module.exports = {
  'MessageModel' : MessageModel
};

