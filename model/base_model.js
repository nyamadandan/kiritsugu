var mongoose = require('mongoose');

var Fiber = require('fibers');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var BaseModel = function() {
  BaseModel.prototype.initialize.apply(this, arguments);
};

BaseModel.prototype = {
  initialize: function(connection, modelname, schema) {
    this.schema = schema;
    this.DbModel = (connection || mongoose).model(modelname, schema);
  },

  now: function() {
    return new Date();
  },

  random2d: function() {
    return [Math.random(), Math.random()];
  },

  write: function(values, opt) {
    if (!opt) { opt = { } }
    var callback = opt.callback || null;
    var dbModel = new this.DbModel(values);

    dbModel.save(function(err) {
      if (callback) {
        callback(err);
      }
    });
  },

  find: function(cond, opt) {
    this.DbModel.find.apply(this.DbModel, arguments);
  }
};

module.exports = {
  'BaseModel' : BaseModel
};

