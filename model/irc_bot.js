var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var Fiber = require('fibers');

var util = require('../util');
var BaseModel = require('./base_model').BaseModel;

var IrcBotModel = function() {
  IrcBotModel.prototype.initialize.apply(this, arguments);
};

IrcBotModel.prototype = {
  initialize : function(connection) {
    var schema = new Schema({
      host : {
        type : String,
        default : '127.0.0.1',
        required : true
      },
      port : [{
        type : Number,
        defalut : 6667,
        required : true
      }],
      username : {
        type : String,
        default : 'kiritsugu',
        required : true
      },
      nickname : {
        type : String,
        default : 'kiritsugu', 
        required : true
      },
      realname : {
        type : String,
        default : 'Kiritsugu EMIYA',
        required : true
      },
      channels : [{
        type : String,
        defalut : [],
        required : true
      }],
      charset : {
        type : String,
        default : 'ISO-2022-JP',
        required : true
      },
      insert_date: {
        type: Date,
        default : BaseModel.prototype.now,
        required : true
      }
    });

    BaseModel.prototype.initialize.apply(this, [connection, 'irc_bot', schema]);
  }
}
util.inherit(IrcBotModel, BaseModel);

module.exports = {
  'IrcBotModel' : IrcBotModel
};

