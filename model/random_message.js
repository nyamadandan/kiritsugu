var mongoose = require('mongoose');

var Fiber = require('fibers');
var Schema = mongoose.Schema;
var Query = mongoose.Query;
var ObjectId = Schema.ObjectId;

var util = require('../util');
var BaseModel = require('./base_model').BaseModel;

var SegmentModel = require('./segment').SegmentModel;

var RandomMessageModel = function() {
  RandomMessageModel.prototype.initialize.apply(this, arguments);
};

RandomMessageModel.prototype = {
  initialize : function(connection) {
    var schema = new Schema({
      message : {
        type: String, 
        unique : true,
        required : true
      },

      random : {
        type: Array, 
        default : BaseModel.prototype.random2d,
        index : '2d',
        required : true
      },

      count : {
        type: Number, 
        default : 0,
        required : true
      },

      update_date: {
        type: Date,
        default : BaseModel.prototype.now
      }
    });

    BaseModel.prototype.initialize.apply(this, [connection, 'random_message', schema]);
  },

  selectRandomMessage : function(callback) {
    var cond = {
      random : {$near : BaseModel.prototype.random2d()}
    }

    var field = {
      _id : 1,
      message : 1,
      update_date : 1
    }

    this.DbModel.find(cond).limit(1).exec(function(err, docs) {
      if(callback) {
        callback(err, docs);
      }
    });
  },

  write : function(value, opt) {
    if(!opt){
      opt = {};
    }
    var callback = opt.callback;
    var message = '';

    if (typeof value === 'string') {
      message = value;
    } else {
      for(var i = 0; i < value.length; ++i) {
        message = message + value[i].segment;
      }
      message = message + value[value.length - 1].next_segment;
    }

    var cond = {
      message : message
    };

    var update = {
      message : message,
      random : BaseModel.prototype.random2d(),
      update_date : BaseModel.prototype.now(),
      $inc : {count : 1}
    };

    var option = {
      upsert : true
    };

    this.DbModel.update(cond, update, option, function(err) {
      if(callback) {
        callback(err);
      }
    });
  }
};

util.inherit(RandomMessageModel, BaseModel);

module.exports = {
  'RandomMessageModel' : RandomMessageModel
};

