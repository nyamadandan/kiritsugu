var mongoose = require('mongoose');

var Fiber = require('fibers');
var Schema = mongoose.Schema;
var Query = mongoose.Query;
var ObjectId = Schema.ObjectId;

var util = require('../util');
var log = util.log;

var BaseModel = require('./base_model').BaseModel;

var SegmentModel = function() {
  SegmentModel.prototype.initialize.apply(this, arguments);
};

SegmentModel.prototype = {
  initialize : function(connection, maxRandomMessage) {
    var schema = new Schema({
      segment: {
        type: String, 
        required : true
      },

      next_segment: {
        type: String,
        required : true
      },

      count : {
        type: Number, 
        default : 0,
        required : true
      },

      start_count : {
        type: Number, 
        default : 0,
        required : true
      },

      end_count : {
        type: Number, 
        default : 0,
        required : true
      },

      random : {
        type: Array, 
        default : BaseModel.prototype.random2d,
        index : '2d',
        required : true
      },

      froms : [{
        type: String, 
      }],

      channels : [{
        type: String, 
      }],

      update_date: {
        type: Date,
        default : BaseModel.prototype.now
      }
    });
    schema.index({segment : 1, next_segment : 1}, {unique : true});

    BaseModel.prototype.initialize.apply(this, [connection, 'segment', schema]);

    this.maxRandomMessage = maxRandomMessage || 100;
  },

  selectRandomNextSegment : function(segment, callback) {
    var cond = {
      random : {$near : BaseModel.prototype.random2d()},
      segment : segment
    }

    var field = {
      _id : 1,
      segment : 1,
      next_segment : 1,
      count : 1,
      start_count : 1,
      end_count : 1,
      update_date : 1
    }

    this.DbModel.find(cond).limit(1).exec(function(err, docs) {
      if(callback) {
        callback(err, docs);
      }
    });
  },

  selectRandomStartSegment : function(callback) {
    var cond = {
      random : {$near : BaseModel.prototype.random2d()},
      start_count : { $gt : 0 }
    }

    var field = {
      _id : 1,
      segment : 1,
      next_segment : 1,
      count : 1,
      start_count : 1,
      end_count : 1,
      update_date : 1
    }

    this.DbModel.find(cond).limit(1).exec(function(err, docs) {
      if(callback) {
        callback(err, docs);
      }
    });
  },

  writeMarkov : function(markov, opt) {
    if(!opt) {
      opt = {};
    }

    var callback = opt.callback;
    var from = opt.from || null;
    var channel = opt.channel || null;

    for( var segment in markov) {
      if(!markov.hasOwnProperty(segment)) {
        continue;
      }

      for( var nextSegment in markov[segment].next ) {
        if(!markov[segment].next.hasOwnProperty(nextSegment)) {
          continue;
        }

        (function(that, segment, nextSegment){
          var cond = {
            segment : segment,
            next_segment : nextSegment
          }

          var update = {};
          update.next_segment = nextSegment

          update.$inc = {};
          update.$inc.count = 1;
          if (markov[segment].next[nextSegment].start) {
            update.$inc.start_count = 1;
          }
          if (markov[segment].next[nextSegment].end) {
            update.$inc.end_count = 1;
          }
          update.update_date = BaseModel.prototype.now();
          update.random = BaseModel.prototype.random2d();

          update.$addToSet = {}
          if (from) {
            update.$addToSet.froms = from;
          }
          if (channel) {
            update.$addToSet.channels = channel;
          }

          var option = {};
          option.upsert = true;

          that.DbModel.update(cond, update, option, function(err) {
            if(err) {
              log.error(err.toString());
            }
          });
        })(this, segment, nextSegment);
      }
    }
  }
};

util.inherit(SegmentModel, BaseModel);

module.exports = {
  'SegmentModel' : SegmentModel
};

