module.exports = {
  'MessageModel' : require('./message').MessageModel,
  'IrcBotModel' : require('./irc_bot').IrcBotModel,
  'SegmentModel' : require('./segment').SegmentModel,
  'RandomMessageModel' : require('./random_message').RandomMessageModel
};

