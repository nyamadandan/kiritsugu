/*
 * GET home page.
 */
var util = require('../util');
var model = require('../model');
var IrcBotModel = model.IrcBotModel;
var SegmentModel = model.SegmentModel;
var MessageModel = model.MessageModel;
var RandomMessageModel = model.RandomMessageModel;

var log = util.log;

module.exports = function(config) {
  var routes = {};

  routes.show = function(req, res) {
    var segmentModel = new SegmentModel();

    segmentModel.find(function(err, docs) {
      if (err) {
        log.error(err.toString());
        res.status(500);
        res.render('error', {config: config });
        return;
      }

      var renderParams = {
        config: config,
        segments: docs
      };

      res.render('segments', renderParams);
    });
  }

  return routes;
};

