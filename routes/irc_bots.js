/*
 * GET home page.
 */
var util = require('../util');
var model = require('../model');
var IrcBotModel = model.IrcBotModel;
var SegmentModel = model.SegmentModel;
var MessageModel = model.MessageModel;
var RandomMessageModel = model.RandomMessageModel;

var log = util.log;

module.exports = function(config) {
  var routes = {};

  routes.get = function(req, res) {
    var actions = {
      show: routes.show,
      create: routes.get_create
    };

    var action = actions[req.params.action];

    if (!action) {
      return routes.error(req, res);
    }

    return action(req, res);
  };

  routes.post = function(req, res) {
    var actions = {
      create: routes.post_create
    };

    var action = actions[req.params.action];

    if (!action) {
      return routes.error(req, res);
    }

    return action(req, res);
  };

  routes.error = function(req, res, err) {
    res.status(500);
    res.render('error', {config: config });
  };

  routes.show = function(req, res) {
    var ircBotModel = new IrcBotModel();

    ircBotModel.find(function(err, docs) {
      if (err) {
        log.error(err.toString());
        return routes.error(req, res);
      }

      var renderParams = {
        config: config,
        infoBots: docs
      };

      res.render('irc_bots_show', renderParams);
    });
  };

  routes.get_create = function(req, res) {
    var ircBotModel = new IrcBotModel();

    ircBotModel.find(function(err, docs) {
      if (err) {
        log.error(err.toString());
        return routes.error(req, res);
      }

      var renderParams = {
        config: config,
        infoBots: docs
      };

      res.render('irc_bots_create', renderParams);
    });
  };

  routes.post_create = function(req, res) {
    var errors = [];

    var host = req.param('host');
    if (host.length === 0) {
      host = 'localhost';
    }
    if (!/^[\w\.]+$/.test(host)) {
      errors.push('host');
    }

    var charset = req.param('charset');
    if (charset.length === 0) {
      charset = 'kiritsugu';
    }
    if (!/^[\w\-]+$/.test(charset)) {
      errors.push('charset');
    }

    var nickname = req.param('nickname');
    if (nickname.length === 0) {
      nickname = 'kiritsugu';
    }
    if (!/^[\w\-]+$/.test(nickname)) {
      errors.push('nickname');
    }

    var username = req.param('username');
    if (username.length === 0) {
      username = 'kiritsugu';
    }
    if (!/^[\w\-]+$/.test(username)) {
      errors.push('username');
    }

    var realname = req.param('realname');
    if (realname.length === 0) {
      realname = 'Kiritsugu EMIYA';
    }
    if (!/^[\w\- ]+$/.test(realname)) {
      errors.push('realname');
    }

    var channel = req.param('channel');
    if (channel.length === 0) {
      channel = '#kiritsugu';
    }
    if (!/^#\w+$/.test(channel)) {
      errors.push('channel');
    }

    var port = req.param('port');
    if (port.length === 0) {
      port = '6667';
    }
    if (!/^[\+-]?\d+$/.test(port)) {
      errors.push('port');
    }

    if (errors.length > 0) {
      return routes.error(req, res, errors);
    }

    var arg = {
      host: host,
      charset: charset,
      nickname: nickname,
      username: username,
      realname: realname,
      channels: [channel],
      port: Math.floor(parseInt(port, 10))
    };

    (function(req, res) {

      var ircBotModel = new IrcBotModel();
      ircBotModel.write(arg, {
        callback: function(err) {
          if (err) {
            log.error(err.toString());
            return routes.error(req, res, ['db write error']);
          }

          return routes.show(req, res);
        }
      });
    })(req, res);
  };

  return routes;
};

