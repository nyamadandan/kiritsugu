/*
 * GET home page.
 */
var util = require('../util');
var model = require('../model');
var IrcBotModel = model.IrcBotModel;
var SegmentModel = model.SegmentModel;
var MessageModel = model.MessageModel;
var RandomMessageModel = model.RandomMessageModel;

var log = util.log;

var messages = require('./messages');
var segments = require('./segments');
var random_messages = require('./random_messages');
var irc_bots = require('./irc_bots');

module.exports = function(config) {
  var routes = {};

  routes.index = function(req, res) {
    res.render('index', { config: config });
  };

  routes.messages = messages(config);
  routes.segments = segments(config);
  routes.irc_bots = irc_bots(config);
  routes.random_messages = random_messages(config);

  return routes;
};

