/*
 * GET home page.
 */
var util = require('../util');
var model = require('../model');
var IrcBotModel = model.IrcBotModel;
var SegmentModel = model.SegmentModel;
var MessageModel = model.MessageModel;
var RandomMessageModel = model.RandomMessageModel;

var log = util.log;

module.exports = function(config) {
  var routes = {};

  routes.show = function(req, res) {
    var messageModel = new MessageModel();

    var result = messageModel.find(function(err, docs) {
      if (err) {
        log.error(err.toString());
        res.status(500);
        res.render('error', {config: config });
        return;
      }

      var renderParams = {
        config: config,
        messages: docs
      };

      res.render('messages', renderParams);
    });
  };

  return routes;
};

